build:
	docker build -t yourname/react-project .

start:
	docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3000:3000 -e CHOKIDAR_USEPOLLING=true yourname/react-project 

